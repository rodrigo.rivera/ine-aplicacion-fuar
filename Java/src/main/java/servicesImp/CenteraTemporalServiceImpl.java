package servicesImp;

/**
 * CenteraService.java
 * Fecha de creación: 23/08/2016
 *
 * Copyright (c) 2016 Instituto Nacional Electoral. Dirección
 * Ejecutiva del Registro Federal de Electores.
 * Periférico Sur 239, México, D.F., C.P. 01010.
 * Todos los derechos reservados.
 *
 * Este software es información confidencial, propiedad del
 * Instituto Federal Electoral. Esta información confidencial
 * no deberá ser divulgada y solo se podrá utilizar de acuerdo
 * a los términos que determine el propio Instituto.
 */


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.SecureRandom;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

//import mx.ine.rfe.siirfe.aplicaciones.reincorporacion.servicio.comun.CenteraService;


/**
 * Clase que implementa la interfaz {@link CenteraService}
 * @author victor.lopezg@ine.mx
 * @version 1.0
 * @since SIIRFE 6.3
 */
@Service("centeraService")
@Transactional
public class CenteraTemporalServiceImpl implements CenteraService {

	private static final Logger logger = ESAPI.getLogger(CenteraTemporalServiceImpl.class);

    private SecureRandom            random      = new SecureRandom();
    private static final String     UNO         = "Uno.jpg";
    private static final String     DOS         = "Dos.jpg";
    private static final String     TRES        = "Tres.jpg";
    private static final String     CUATRO      = "Cuatro.jpg";
    private static final String     CENTERA_EXT = ".centera";
    // private static final String     TEMP_DIR    = "/tmp/";
    
    @Value("#{propiedadesCentera['dummyCenteraDir']}")
    private String                  outputDir;
    
    /* La documentaciï¿½n de este mï¿½todo se encuentra en la clase o interfaz que
     * lo declara (non-Javadoc)
     * @see mx.org.ife.rfe.siirfe.repositorios.centera.service.CenteraService#enviaArchivo(byte[])
     */
    /**
     * Guarda el arreglo de bytes dado por file en el directorio indicado por la propiedad
     * dummyCenteraDir de el archivo de propiedades de centera. Si esta no existe, guarda el
     * archivo en /tmp/ <br>
     * El nombre del archivo tiene la extension .centera
     * @see CenteraService
     */
    @Override
    public String enviaArchivo(byte[] file) {
        
        String clip;
        String dir;
        
        System.out.println("Output dir: " + outputDir );
        logger.debug(Logger.EVENT_UNSPECIFIED, "Output dir: " + outputDir/*"Output dir: {}", outputDir*/);
        
        if (outputDir != null) {
            dir = outputDir;
        } else {
            // dir = TEMP_DIR;
        	dir = System.getProperty("java.io.tmpdir") + File.separatorChar;
        }
        
        System.out.println("Dir: " + outputDir );
        logger.debug(Logger.EVENT_UNSPECIFIED, "Dir: " + outputDir/*"Dir: {}", outputDir */);
        
        File f;
        FileOutputStream fos = null;
        try {
            while (true) {
                clip = new BigInteger(130, random).toString();
                f = new File(dir + clip + CENTERA_EXT);
                
                if (f.createNewFile()) {
                    fos = new FileOutputStream(f);
                    fos.write(file);
                    
                    break;
                }
            }
        } catch (IOException e) {
            
        	logger.always(Logger.EVENT_FAILURE, "Error-->>", e);
            return null;
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    
                	logger.always(Logger.EVENT_FAILURE, "Error-->>", e);
                    return null;
                }
            }
        }
        
        System.out.println("Regresando clip: " + clip );
        //logger.debug("Regresando clip: " + clip );
        logger.debug(Logger.EVENT_UNSPECIFIED, "Regresando clip: " + clip);
        
        return clip;
        
    }
    
    /* La documentaciï¿½n de este mï¿½todo se encuentra en la clase o interfaz que
     * lo declara (non-Javadoc)
     * @see mx.org.ife.rfe.siirfe.repositorios.centera.service.CenteraService#obtenerArchivo(java.lang.String)
     */
    /**
     * Si existe el archivo en el directorio de centera definido por la propiedad
     * dummyCenteraDir del archivo de propiedades de centera o si esta no exsite, /tmp/,
     * devuelve el contenido de dicho archivo. <br>
     * En caso contrario, si el mï¿½dulo 5 del clip es 0 devuelve null. Si es distinto de 0,
     * devuelve el contenido de uno de 4 archivos de imï¿½gen contenidos como recurso interno
     */
    @SuppressWarnings("resource")
	@Override
    public byte[] obtenerArchivo(String clip) {
        String dir;
        
        System.out.println("outputDir: " + outputDir );
        logger.debug(Logger.EVENT_UNSPECIFIED, "outputDir: " + outputDir );
        
        if (outputDir != null) {
            dir = outputDir;
        } else {
        	// dir = TEMP_DIR;
        	dir = System.getProperty("java.io.tmpdir") + File.separatorChar;
        }
        File archivoF = new File(dir + clip + CENTERA_EXT);
        InputStream streamImagen;
        
        try {
            if (archivoF.exists()) {
                
                streamImagen = new FileInputStream(archivoF);
            } else {
                String archivo;
                
                long num = Long.parseLong(clip.substring(0, 10), 36) % 4;
                
                switch ((int) num) {
                    case 1:
                        archivo = UNO;
                        break;
                    case 2:
                        archivo = DOS;
                        break;
                    case 3:
                        archivo = TRES;
                        break;
                    case 4:
                        archivo = CUATRO;
                        break;
                    default:
                        return null;
                }
                streamImagen = getClass().getResourceAsStream("/" + archivo);
            }
            
            byte[] buffer = new byte[1024];
            int leidos = 0;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            
            while ((leidos = streamImagen.read(buffer)) >= 0) {
                baos.write(buffer, 0, leidos);
            }
            return baos.toByteArray();
        } catch (IOException e) {
            
        	logger.always(Logger.EVENT_FAILURE, "Error-->>", e);
            return null;
        }
        
    }

}