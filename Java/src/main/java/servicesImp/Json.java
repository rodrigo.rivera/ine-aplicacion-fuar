package servicesImp;


public class Json {
 
    private String url ;
    private int id;

    public Json() {
    }

    public Json(String url, int id) {
        this.url = url;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "{id:"+id+" , url:"+url;
    }       
}
