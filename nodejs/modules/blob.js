var oracledb = require('oracledb');
var async = require('async');
var fs = require('fs');
var dbConfig = require('../config/dbconfig')


// Stream a LOB to a file
async function doStream(lob, outFileName) {

    const doStreamHelper = new Promise((resolve, reject) => {
        //console.log("tipo de lob== " + lob.type);
        if (lob.type === oracledb.CLOB) {
            //console.log('Writing a CLOB to ' + outFileName);
            lob.setEncoding('utf8'); // set the encoding so we get a 'string' not a 'buffer'
        } else {
            //console.log('Writing a BLOB to ' + outFileName);
        }
        let errorHandled = false;
        lob.on('error', (err) => {
            console.log("lob.on 'error' event");
            if (!errorHandled) {
                errorHandled = true;
                lob.close(() => {
                    reject(err);
                });
            }
        });
        lob.on('end', () => {
            // console.log("lob.on 'end' event");
        });
        lob.on('close', () => {
            // console.log("lob.on 'close' event");
            if (!errorHandled) {
                resolve();
            }
        });
        const outStream = fs.createWriteStream("public/images/" + outFileName);
        outStream.on('error', (err) => {
            //console.log("outStream.on 'error' event");
            if (!errorHandled) {
                errorHandled = true;
                lob.close(() => {
                    reject(err);
                });
            }
        });
        // Switch into flowing mode and push the LOB to the file
        lob.pipe(outStream);
    });
    await doStreamHelper;
}

/**
 * 
 * @param {*} sql 
 * @param {*} type 
 * @param {*} name 
 * @param {*} json 
 * @param {*} callback 
 */
async function run(sql, base, type, name, json, callback) {
    const firm = "[blob::run] ";
    //console.log(firm + sql)
    let connection;
    try {
        connection = await oracledb.getConnection(dbConfig.base[base]);
        result = await connection.execute(sql);
        if (result.rows.length === 0) {
            throw new Error("No results.  Did you run lobinsert1.js?");
        }
        lob = result.rows[0][0];
        if (lob === null) {

            json[type] = { status: 400, descripcion: type + "-" + name };
            //console.log(JSON.stringify(json));
            callback(true, json);

            //throw new Error("LOB was NULL");
        } else {

            await doStream(lob, type + "-" + name);
            var direccion = "http://" + dbConfig.imagenes.ip + ":" + dbConfig.imagenes.puerto + "/public/images/"
            json[type] = { status: 200, descripcion: direccion + type + "-" + name };
            //console.log(JSON.stringify(json));
            callback(false, json);
        }
    } catch (err) {
        //json[type] = { status: 1, descripcion: "No esta disponible" };
        callback(true, json);
        console.error(err);
    } finally {
        if (connection) {
            try {
                await connection.close();
            } catch (err) {
                // console.error(err);
            }
        }
    }
}
exports.run = run;