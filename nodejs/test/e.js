
var oracledb = require('oracledb');
var async = require('async');
var fs = require('fs');
var dbConfig = require('../DBManager/dbconfig')


// Stream a LOB to a file
async function doStream(lob, outFileName) {

    const doStreamHelper = new Promise((resolve, reject) => {

        if (lob.type === oracledb.CLOB) {
            console.log('Writing a CLOB to ' + outFileName);
            lob.setEncoding('utf8');  // set the encoding so we get a 'string' not a 'buffer'
        } else {
            console.log('Writing a BLOB to ' + outFileName);
        }

        let errorHandled = false;

        lob.on('error', (err) => {
            // console.log("lob.on 'error' event");
            if (!errorHandled) {
                errorHandled = true;
                lob.close(() => {
                    reject(err);
                });
            }
        });
        lob.on('end', () => {
            // console.log("lob.on 'end' event");
        });
        lob.on('close', () => {
            // console.log("lob.on 'close' event");
            if (!errorHandled) {
                resolve();
            }
        });

        const outStream = fs.createWriteStream(outFileName);
        outStream.on('error', (err) => {
            // console.log("outStream.on 'error' event");
            if (!errorHandled) {
                errorHandled = true;
                lob.close(() => {
                    reject(err);
                });
            }
        });

        // Switch into flowing mode and push the LOB to the file
        lob.pipe(outStream);
    });

    await doStreamHelper;
}

async function run() {
    let connection;

    try {
        connection = await oracledb.getConnection(dbConfig);

        // //
        // // Fetch a CLOB and stream it
        // //
        // let result = await connection.execute(`select  CEPH.GET_NOTIFICACION_FUAR_NACIONAL('485056218','pv') FROM DUAL`);
        // if (result.rows.length === 0) {
        //   throw new Error("No results.  Did you run lobinsert1.js?");
        // }
        // let lob = result.rows[0][0];
        // if (lob === null) {
        //   throw new Error("LOB was NULL");
        // }
        // await doStream(lob, 'clobstream1out.txt');

        //
        // Fetch a BLOB and stream it
        //
        result = await connection.execute(`select  CEPH.GET_FOTO('200000022474','pv') FROM DUAL`);
        if (result.rows.length === 0) {
            throw new Error("No results.  Did you run lobinsert1.js?");
        }
        lob = result.rows[0][0];
        if (lob === null) {
            throw new Error("LOB was NULL");
        }
        await doStream(lob, 'blobstream1out.jpg');

    } catch (err) {
        console.error(err);
    } finally {
        if (connection) {
            try {
                await connection.close();
            } catch (err) {
                console.error(err);
            }
        }
    }
}

run();