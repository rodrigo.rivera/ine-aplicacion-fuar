package servicesImp;

import services.Imagen;

import org.jnbis.api.Jnbis;
import org.json.simple.JSONObject;
import org.owasp.esapi.errors.EncodingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import mx.org.ife.rfe.siirfe.comun.modelo.catalogo.mediosidentificacion.TipoMedioIdentificacion;
import mx.org.ife.rfe.siirfe.comun.modelo.entidad.mediosidentificacion.digitalizacion.ProcesoDigitalizacion;
import mx.org.ife.siirfe.comun.dao.mediosidentificacion.digitalizacion.ProcesoDigitalizacionDAO;
//import mx.org.ife.siirfe.comun.dao.mediosidentificacion.digitalizacion.

import java.io.InputStream;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import org.owasp.esapi.Encoder;
import org.owasp.esapi.reference.DefaultEncoder;


@Service
public class ImgImpl implements Imagen{

	
	@Autowired
	@Qualifier("centeraService")
	private CenteraService centeraService;
	
	@Autowired
	ProcesoDigitalizacionDAO procesoDigitalizacionDAO ;//= new ProcesoDigitalizacionDAO();
	private String user = "admin";
	
	
//	@Autowired
//	private NotificacionRepositorioService notificacionRepositorioService;
	
	@Override
	public JSONObject converteSqltoJPG(String url) {
		// x
		JSONObject jo = new JSONObject(); 
		File file = new File(url);
		System.out.println("+++++++++"+url);
		if(file.exists()) {
			
	        String send =url+".jpg"; 
			File jpg = Jnbis.wsq().decode(url).toJpg().asFile(send);
			jo = new JSONObject();
			jo.put("status", 200);
			jo.put("descripcion",send);				
			return jo;
		}else {			
			jo = new JSONObject();
			jo.put("status", 400);
			jo.put("descripcion","Error al generar la imagen");
			return jo;	
		}
		
		
	}

	@Override
	public byte[] getMedioIdentificacionByFuar(String fuar, TipoMedioIdentificacion tmi) {
		// TODO Auto-generated method stub
		//System.out.println(procesoDigitalizacionDAO.obtenProcesoDigitalizacionByFolio(fuar));
		try {
			

		System.out.println("-----------------"+fuar);
		System.out.println("-------------------------"+procesoDigitalizacionDAO);
		ProcesoDigitalizacion digitalizacion  = procesoDigitalizacionDAO.obtenProcesoDigitalizacionByFolio(fuar);
		} catch (Exception e) {
			System.out.println(e);
		}			
		
		//System.out.println(digitalizacion);
//		if(digitalizacion != null){
//			//logger.info(Logger.EVENT_SUCCESS, "ABR: getMedioIdentificacionByFuar digitalizacion!=null");
//			System.out.println("ABR: getMedioIdentificacionByFuar digitalizacion!=null");
//
//			
//			ImagenComprobante imagenComprobante = new ImagenComprobante();
//			imagenComprobante.setProcesoDigitalizacionID(digitalizacion.getId());
//			imagenComprobante.setNumeroImagen(1);
//			imagenComprobante.setTipoMedioIdentificacion(tmi);
//			//imagenComprobante = nrExpedienteDAO.obtenDatosImagenComprobante(imagenComprobante);
//			
//			if(imagenComprobante != null){
//				//logger.info(Logger.EVENT_SUCCESS, "\n REINC: PASO1 - Buscar por CLIP centera");
//				System.out.println("REINC: PASO1 - Buscar por CLIP centera");
//				System.out.println(imagenComprobante.getClip());
//				if(!estaVacio(imagenComprobante.getClip())){
//					//logger.info(Logger.EVENT_SUCCESS, "Clip obtenido: " + imagenComprobante.getClip());
//					return centeraService.obtenerArchivo(imagenComprobante.getClip());
//				}
//				
//				///logger.info(Logger.EVENT_SUCCESS, "\n REINC: PASO2- Buscar por PATH Centera");
//				if(!estaVacio(imagenComprobante.getPathRegistroContingencia())){
//					String dir = imagenComprobante.getPathRegistroContingencia();
//					
//					try {
//						dir = sanitize(dir);
//					} catch (EncodingException e1) {
//						// TODO Auto-generated catch block
//						e1.printStackTrace();
//					}
//					
//					try{
//						File archivoF = new File(dir);
//						InputStream streamImagen;
//						
//						if(user.equals("admin")){
//							if(archivoF.exists()){
//								streamImagen = new FileInputStream(archivoF);
//								byte[] arreglo = new byte[1024];
//								int leidos = 0;
//								ByteArrayOutputStream baos = new ByteArrayOutputStream();
//								
//								while((leidos = streamImagen.read(arreglo)) >= 0){
//									baos.write(arreglo, 0, leidos);
//								}
//								return baos.toByteArray();
//							}
//						}
//
//					}
//					catch(IOException e){
//						//logger.always(Logger.EVENT_FAILURE, "Error ->", e);
//						System.out.println("Error "+e);
//						return null;
//						
//					}
//				}
//				
//				//logger.info(Logger.EVENT_SUCCESS, "\n REINC: PASO3- Buscar por CEPH");
//				System.out.println();
//				if(imagenComprobante.getEnCeph() != null && imagenComprobante.getEnCeph()){
//					return notificacionRepositorioService.recuperaImagenComprobante(tmi, fuar, 1);
//				}
//				
//			}
//			
//		}
		return null;
	}
	
	public static boolean estaVacio(String cadena){
		if(cadena == null || cadena.isEmpty()){
			return true;
		}else return false;
	}
	
	public String sanitize(String url) throws EncodingException {

		Encoder encoder = new DefaultEncoder(new ArrayList<String>());
		// No se que sea canonicalize, pero es lo primero que se hace
		String clean = encoder.canonicalize(url).trim();
		// Decodificar url
		clean = encoder.decodeFromURL(clean);

		// Detecta y elimina saltos de linea y retornos de carro para evitar la divisin
		// de la respuesta HTTP
		int idxR = clean.indexOf('\r');
		int idxN = clean.indexOf('\n');

		if (idxN >= 0 || idxR >= 0) {
			if (idxN > idxR) {
				clean = clean.substring(0, idxN - 1);
			} else {
				clean = clean.substring(0, idxR - 1);
			}
		}

		// Volver a decodificar de nuevo
		return encoder.encodeForURL(clean);
	}
}

