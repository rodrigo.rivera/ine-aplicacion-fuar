var oracledb = require('oracledb');
var fs = require('fs');
var dbConfig = require('../DBManager/dbconfig')
var query = require("../DBManager/query")
var connection = {
    user: dbConfig.user,
    password: dbConfig.password,
    connectString: dbConfig.connectString
};

/**
 *  documento 
 */
function getXML() {
    console.log("paso 1")
    var notificacion = "485056218";
    var sql = "select  CEPH.GET_NOTIFICACION_FUAR_NACIONAL('485056218','pv') FROM DUAL";
    var outFileName = notificacion + "rodrigo" + '.xml';

    query.ejecute(sql, function (err, result) {
        if (err) {
            console.error(result)
        } else {
            console.log("exito: " + result);
        }
    });


}

var dostream = function (lob, cb) {
    console.log("paso 3")
    //console.log("antes construir "+JSON.stringify(lob));
    if (lob.type === oracledb.CLOB) {
        console.log('Writing a CLOB to ' + outFileName);
        lob.setEncoding('utf8');  // set the encoding so we get a 'string' not a 'buffer'
    } else {
        console.log('Writing a BLOB to ' + outFileName);
    }

    var errorHandled = false;
    console.log("des construir " + JSON.stringify(lob));
    lob.on(
        'error',
        function (err) {
            console.log("lob.on 'error' event");
            if (!errorHandled) {
                errorHandled = true;
                lob.close(function () {
                    return cb(err);
                });
            }
        });
    lob.on(
        'end',
        function () {
            console.log("lob.on 'end' event");
        });
    lob.on(
        'close',
        function () {
            // console.log("lob.on 'close' event");
            if (!errorHandled) {
                return cb(null);
            }
        });

    var outStream = fs.createWriteStream(outFileName);
    outStream.on(
        'error',
        function (err) {
            console.log("outStream.on 'error' event");
            if (!errorHandled) {
                errorHandled = true;
                lob.close(function () {
                    return cb(err);
                });
            }
        });

    // Switch into flowing mode and push the LOB to the file
    lob.pipe(outStream);
};