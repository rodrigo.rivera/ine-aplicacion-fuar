var express = require('express');
var router = express.Router();
var controller = require("../modules/controller")

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Folio Fuar ' });
});


router.post('/search-fuar', function(req, res, next) {
    const firm = "[Router::search-fuar]"
    if (req.body != null && parseInt(req.body.id) > 0) {
        //console.log(firm + "entro")
        controller.getData(req.body.id, function(err, result) {
            if (err) {
                res.send({
                    data: result
                });
            } else {

                res.send({
                    data: result
                });
            }
        });
    } else {
        res.send({
            code: 10,
            descripcion: "caractes invalidos"
        })
    }
});


/*******************************
 * API
 *******************************/

router.post('/foto', function(req, res, next) {
    const firm = "[Router::foto]"
    console.log(firm + JSON.stringify(req.body.fuar));
    if (req.body != null) {
        controller.apiFoto(req.body.fuar, req.body.base, function(err, result) {
            //console.log("rouyter " + JSON.stringify(result))
            res.send(result);
        });
    } else {
        res.send({
            foto: {
                code: 400,
                descripcion: "caractes invalidos"
            }
        });
    }
});



router.post('/xml', function(req, res, next) {
    const firm = "[Router::foto]"
        //console.log(firm + JSON.stringify(req.body.fuar));
    if (req.body != null) {
        controller.apiXML(req.body.fuar, req.body.base, function(err, result) {
            res.send(result);
        });
    } else {
        res.send({
            XML: {
                code: 403,
                descripcion: "caractes invalidos"
            }

        });
    }
});


router.post('/firma', function(req, res, next) {
    const firm = "[Router::firma]"
        //console.log(firm + JSON.stringify(req.body.fuar));
    if (req.body != null) {
        controller.apiFirma(req.body.fuar, req.body.base, function(err, result) {
            res.send(result);
        });
    } else {
        res.send({
            XML: {
                code: 403,
                descripcion: "caractes invalidos"
            }

        });
    }
});



router.post('/hisotrial', function(req, res, next) {
    const firm = "[Router::hisotrial]"
    console.log("-----------------entro")
    console.log(req.body)
    console.log("-----------------salgo")
    if (req.body != null) {
        //console.log(firm + "entro")
        controller.apiHistorial(req.body.fuar, req.body.base, function(err, result) {
            res.send(result);
        });
    } else {
        res.send({
            hisotrial: {
                code: 10,
                descripcion: "caractes invalidos"
            }
        })
    }
});


router.post('/huellas', function(req, res, next) {
    const firm = "[Router::huellas]"
    if (req.body != null) {
        //console.log(firm + "entro")
        controller.apiHuellas(req.body.fuar, req.body.base, function(err, result) {
            res.send(result);
        });
    } else {
        res.send({
            huellas: {
                code: 10,
                descripcion: "caractes invalidos"
            }
        })
    }
});

router.post('/wsq/huellas', function(req, res, next) {
    const firm = "[Router::wsq/huellas]"
        // console.log(firm + "----[" + Object.keys(req.body) + "]")
        // console.log(firm + "----[" + Object.keys(req.body) + "]")
        // console.log(firm + "entro" + req.body.fuar)
    if (req.body != null) {
        console.log(firm + "entro" + req.body.fuar)
        controller.apiHuellasWSQ(req.body.fuar, req.body.base, function(err, result) {
            res.send(result);
        });
    } else {
        res.send({
            huellas: {
                code: 10,
                descripcion: "caractes invalidos"
            }
        })
    }
});

module.exports = router;