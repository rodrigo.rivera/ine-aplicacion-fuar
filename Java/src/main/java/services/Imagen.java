package services;

import org.json.simple.JSONObject;

import mx.org.ife.rfe.siirfe.comun.modelo.catalogo.mediosidentificacion.TipoMedioIdentificacion;

public interface Imagen{

	
	public JSONObject converteSqltoJPG(String url);	
	public byte [] getMedioIdentificacionByFuar(String fuar, TipoMedioIdentificacion tmi);
}
